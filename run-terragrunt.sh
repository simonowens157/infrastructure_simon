#!/bin/bash

docker-compose -f terragrunt.yml up -d
docker-compose -f terragrunt.yml exec terragrunt sh
docker-compose -f terragrunt.yml down