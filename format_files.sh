for d in $(find .); do
  if [[ "$d" == *".git"* ]]; then
    :
  elif  [[ "$d" == *"archived"* ]]; then
    :
  elif  [[ "$d" == *".terraform"* ]]; then
    :
  elif  [[ "$d" == *"terragrunt"* ]]; then
    :
  else
    dos2unix $d
  fi
done