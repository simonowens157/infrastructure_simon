# The produced image can test: terraform, aws, docker, packer, python, golang, and bash code
# this image takes a long time to build
FROM golang:1.12

# install utility software
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
        ca-certificates \
        curl \
        dnsutils \
        jq \
        libcrypt-hcesha-perl \
        python-pip \
        rsyslog \
        software-properties-common \
        sudo \
        vim \
        wget

# get docker for building images
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    apt-get update && \
    apt-get -y install apt-transport-https && \
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/debian \
       $(lsb_release -cs) \
       stable" && \
    apt-get update && \
    apt-get -y install docker-ce

# get terraform
ENV TERRAFORM_VERSION=0.11.13
RUN apt-get install -y unzip ;\
    cd /tmp ;\
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip ;\
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin ;\
    rm -rf /tmp/*

# Get packer
ENV PACKER_VERSION=1.4.0
RUN cd /tmp ;\
    wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip ;\
    unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/bin ;\
    rm -rf /tmp/*

# get terragrunt
RUN go get github.com/gruntwork-io/terragrunt ;\
    CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' github.com/gruntwork-io/terragrunt

# get terratest modules
COPY golang.org /go/src/
COPY google.golang.org /go/src/
COPY github.com /go/src/
RUN go get -u golang.org/x/crypto/...

# get bats for bash testing
RUN apt-get install curl -y ;\
    curl -sL https://deb.nodesource.com/setup_11.x | /bin/bash - ;\
    apt-get install nodejs -y ;\
    npm install -g bats

# get python 3.5
RUN apt-get install python3 -y

# copy over the tests
COPY Dockerfile_test.bats /go

ENTRYPOINT ["/bin/bash"]