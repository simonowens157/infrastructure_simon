#!/bin/bash

# Import config
while read in; do export "$in"; done < config.env

usage() {
    echo ""
	echo "usage: "$0" <build|clean|test|release|run>"
    echo ""
}
build(){
	docker build -t "${REPO}:${TAG}" .
}
test(){
	sh run-terragrunt.sh bats image_test.bats
}
# User must give only 1 argument
if [ "$#" -ne 1 ]; then
    usage
    exit 1
fi
if [[ "$1" == "build" ]]; then
	build
elif [[ "$1" == "clean" ]]; then
	docker rmi ${REPO}:${TAG}
elif [[ "$1" == "test" ]]; then
	docker run --rm --entrypoint bats ${REPO}:${TAG} Dockerfile_test.bats
elif [[ "$1" == "release" ]]; then
	build
	docker push "${REPO}:${TAG}"
	docker push "${REPO}:latest"
elif [[ "$1" == "run" ]]; then
	build
	docker run --rm -it ${REPO}:${TAG}
fi