#!/usr/bin/env bats

# ensure docker exsists
@test "docker exists" {
  run docker --version
}

# ensure terraform exsists
@test "terraform exists" {
  run terraform -v
}

# ensure packer exsists
@test "packer exists" {
  run packer --version
}

# ensure terragrunt exsists
@test "Terragrunt exists" {
  run terragrunt -v
}

# ensure terratest terraform module is installed
@test "terratest terraform module exists" {
  [ -d "/go/src/github.com/gruntwork-io/terratest/modules/terraform/" ]
}

# ensure terratest aws module is installed
@test "terratest aws module exists" {
  [ -d "/go/src/github.com/gruntwork-io/terratest/modules/aws/" ]
}

# ensure terratest docker module is installed
@test "terratest docker module exists" {
  [ -d "/go/src/github.com/gruntwork-io/terratest/modules/docker/" ]
}
# ensure terratest packer module is installed
@test "terratest packer module exists" {
  [ -d "/go/src/github.com/gruntwork-io/terratest/modules/packer/" ]
}

# ensure python 3.5 exsists
@test "python 3.5 exists" {
  run python3 --version
  [ "$status" -eq 0 ]
  [[ "$output" == *"3.5"* ]]
}

# ensure bats exsists
@test "bats exists" {
  run bats -v
}