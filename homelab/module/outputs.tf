output "docker_service_name" {
  value = ["${docker_service.service.name}"]
}
output "docker_volume_name" {
  value = ["${docker_volume.service_volume.*.name}"]
}
output "docker_network_name" {
  value = ["${docker_network.service_network.*.name}"]
}