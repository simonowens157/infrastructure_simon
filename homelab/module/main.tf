provider "docker" {
  host      = "tcp://${var.daemon_host_port}"
  cert_material = "${file(pathexpand("${var.daemon_cert_path}/cert.pem"))}"
  key_material = "${file(pathexpand("${var.daemon_cert_path}/key.pem"))}"
}

resource "docker_volume" "service_volume" {
  count = "${length(var.host_mounts)}"
  name  = "volume-${count.index}"
  driver_opts = {
      type = "none"
      o = "bind"
      device = "${var.host_mounts[count.index]}"
  }
}
resource "docker_network" "service_network" {
  count = "${length(var.networks)}"
  name   = "${var.service_name}-${lookup(var.networks[count.index], "name")}"
  driver = "${lookup(var.networks[count.index], "driver")}"
}
resource "docker_service" "service" {
  name = "${var.service_name}"
  task_spec {
    container_spec {
      image = "${var.image}:${var.tag}"
      labels = "${var.labels}"
      hostname = "${var.service_name}"
      env = "${var.environment_vars}"
      dir    = "${var.workdir}"
      user   = "${var.user}"
      groups = "${var.groups}"
      mounts = "${var.container_mounts}"
      
      stop_signal       = "SIGTERM"
      stop_grace_period = "10s"
      dns_config {
        nameservers = "${var.nameservers}"
      }
    }
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 4
      window       = "10s"
    }
    placement {
      constraints = [
        "node.role==${var.node_type}",
      ]
      prefs = [
        "spread=node.role.${var.node_type}",
      ]
    }
    force_update = 0
    runtime      = "container"
    networks     = ["${docker_network.service_network.id}"]
    log_driver {
      name = "json-file"
      options {
        max-size = "10m"
        max-file = "3"
      }
    }
  }
  mode {
    global = true
    #replicated {
    #  replicas = "${var.replicas}"
    #}
  }
  update_config {
    parallelism       = 2
    delay             = "10s"
    failure_action    = "pause"
    monitor           = "5s"
    max_failure_ratio = "0.1"
    order             = "start-first"
  }
  rollback_config {
    parallelism       = 2
    delay             = "5ms"
    failure_action    = "pause"
    monitor           = "10h"
    max_failure_ratio = "0.9"
    order             = "stop-first"
  }
  endpoint_spec {
    mode = "vip"
    ports {
      target_port    = "${var.container_port}"
      published_port = "${var.host_port}"
      publish_mode   = "ingress"
    }
  }
}