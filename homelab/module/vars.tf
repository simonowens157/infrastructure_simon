variable "daemon_host_port" {
    description = "host:port/  - can you unix socket"
    default = "docker-manager.owens-netsec.com:2376/"
}
variable "daemon_cert_path" {
    description = "Path to docker ca,cert,key.pem"
    default = "../client-certs"
}
variable "image" {
    description = "repo.mycompany.com:8080/foo-service"
}
variable "tag" {
    description = "image tag. ex: v0.0.1"
    default = "latest"
}
variable "service_name" {
    description = "Name of the docker service"
}
variable "container_port" {
    description = "Port inside container"
}
variable "host_port" {
    description = "Port on swarm host"
}
variable "node_type" {
    description = "Worker or Manager node placement"
    default = "worker"
}
variable "nameservers" {
    type = "list"
    description = "192.168.1.10 is my local dns server, the others are generic"
    default = ["192.168.1.10", "1.1.1.1", "8.8.8.8"]
}
variable "replicas" {
    description = "How many instances of this service there are"
    default = 1
}
variable "environment_vars" {
    type = "map"
    description = "Environment variables to be present on the system - map"
}
variable "labels" {
    type = "map"
    description = "Labels for the container, like linux, or traefik labels - map"
}
variable "workdir" {
    description = "Working directory inside the container"
    default = "~/"
}
variable "user" {
    description = "The user running inside the container"
    default = "root"
}
variable "groups" {
    type = "list"
    description = "The groups the user is apart of - list"
    default = ["root", "docker"]
}
variable "container_mounts" {
    type = "list"
    description = "Specifies what will be mounted inside container - list"
}
variable "host_mounts" {
    type = "list"
    description = "Specifies what will be mounted on the host filesystem - list"
}
variable "networks" {
    type = "list"
    description = "A list of networks for the service"
}