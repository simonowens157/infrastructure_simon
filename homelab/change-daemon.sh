#!/bin/bash

# User must give only 1 argument
if [ "$#" -ne 1 ];
then
    echo "Usage: $0 swarm|local"
    exit 1
fi

if [[ "$1" == "swarm" ]]; then  
    setx DOCKER_HOST tcp://docker-manager.owens-netsec.com:2376
    setx DOCKER_TLS_VERIFY 1
    setx DOCKER_CERT_PATH ~/.docker/simon-swarm/
    setx DOCKER_OPTS ""
elif [[ "$1" == "local" ]]; then
    setx DOCKER_HOST tcp:///var/run/docker.sock
    setx DOCKER_TLS_VERIFY 0
    setx DOCKER_CERT_PATH ""
    setx DOCKER_OPTS ""
fi