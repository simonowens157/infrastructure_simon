provider "docker" {
  host      = "tcp://${var.daemon_host_port}"
  cert_material = "${file(pathexpand("${var.daemon_cert_path}/cert.pem"))}"
  key_material = "${file(pathexpand("${var.daemon_cert_path}/key.pem"))}"
}
resource "docker_network" "service_network" {
  count = "${length(var.networks)}"
  name   = "${lookup(var.networks[count.index], "name")}"
  driver = "${lookup(var.networks[count.index], "driver")}"
}