terragrunt = {
  terraform {}
  # Include all settings from the root terraform.tfvars file
  include = {path = "${find_in_parent_folders()}"}
}
networks = [
  {
    name = "gateway"
    driver = "overlay"
  },
  {
    name = "vpn"
    driver = "overlay"
  }
]