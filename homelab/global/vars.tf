variable "daemon_host_port" {
    description = "host:port/  - can you unix socket"
}
variable "daemon_cert_path" {
    description = "Path to docker ca,cert,key.pem"
}
variable "networks" {
    type = "list"
    description = "A list of networks for the service"
}