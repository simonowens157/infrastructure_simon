#!/bin/bash

# Options are "start, stop, build, scan"
usage() {
    echo ""
	echo "usage: "$0" <start|stop|build|test> <service|all> <path>"
    echo "services are named the same as folders in this directory"
    echo "'all' specifies all production docker services, this may take some time"
    echo ""
}
# User must give atleast 2 arguments
if [ "$#" -lt 2 ];
then
    usage
    exit 1
fi

# if given a path
if [ "$#" -eq 3 ];
then
    cd $3
fi

# get vars
while read in; do export "$in"; done < .env

# you can update all services or one
option="$1"
if [[ "$2" == "all" ]]; then    
    echo "Acting on all swarm services"
    # if the folder has a "docker.yml" file then it is a service.
    for D in */; do
        D=${D::-1}
        if [ "$D" == "vol" ] || [ "$D" == "client-certs" ] || [ "$D" == "tests" ]; then
            :
        elif [[ "$1" == "start" ]]; then
            if [ -e "$D/docker-compose.yml" ]; then
                cd $D
                docker stack deploy -c docker-compose.yml $D
                cd ..
            elif [ -e "$D/local.yml" ]; then
                cd $D
                docker-compose -f local.yml up -d
                cd ..
            fi
        elif [[ "$1" == "stop" ]]; then
            if [ -e "$D/local.yml" ]; then
                cd $D
                docker-compose -f local.yml down
                cd ..
            else
                docker stack rm $D
            fi
        elif [[ "$1" == "build" ]]; then
            if [ -e "$D/build-and-push.sh" ]; then
                cd $D
                sh ./build-and-push.sh
                cd ..
            elif [ -e "$D/$D.json" ]; then
                ./build_container.sh $D $D/
            fi
        elif [[ "$1" == "test" ]]; then
            echo "Testing $D"
        fi
    done
# run the commands on just one service
else
    if [[ "$1" == "start" ]]; then
        if [ -e "$2/docker-compose.yml" ]; then
            cd $2
            docker stack deploy -c docker-compose.yml $2
            cd ..
        elif [ -e "$2/local.yml" ]; then
            cd $2
            docker-compose -f local.yml up -d
            cd ..
        fi
        exit 1
    elif [[ "$1" == "stop" ]]; then
        if [ -e "$2/local.yml" ]; then
            cd $2
            docker-compose -f local.yml down
            cd ..
        else
            docker stack rm $2
        fi
        exit 1
    elif [[ "$1" == "build" ]]; then
        docker login ${DTR}
        if [ -e "$2/build-and-push.sh" ]; then
            cd $2
            sh ./build-and-push.sh
            cd ..
        elif [ -e "$2/$2.json" ]; then
            ./build_container.sh $2 $2/
        fi
        exit 1
    elif [[ "$1" == "test" ]]; then
            echo "Testing $2"
    else
        usage
    fi
fi