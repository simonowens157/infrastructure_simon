terragrunt = {
  terraform {source = "../module"}
  # Include all settings from the root terraform.tfvars file
  include = {path = "${find_in_parent_folders()}"}
}
image = "traefik"
tag = "latest"
service_name = "traefik"
container_port = "8080"
host_port = "8080"
environment_vars = {
  env1 = "test"
}
labels = {
  label1 = "test"
}
host_mounts = [
  "/media/vol/traefik", 
  "/media/vol/certs"
]
container_mounts = [
  {
    target    = "/etc/traefik"
    source    = "volume-0"
    type      = "volume"
    read_only = "false"
  },
  {
    target    = "/certs/"
    source    = "volume-1"
    type      = "volume"
    read_only = "false"
  },
]
networks = [
  {
    name = "internal-network-0"
    driver = "overlay"
  }
]
node_type = "manager"
replicas = "1"
# All below Not Required
workdir = ""
user = ""
groups = [""]
nameservers = [""]