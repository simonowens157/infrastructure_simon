terragrunt = {
  terraform {
    source = "git::https://gitlab.com/simonowens157/aws_modules.git//dns?ref=v0.0.2"
  }
  # Include all settings from the root terraform.tfvars file
  include = {
    path = "${find_in_parent_folders()}"
  }
}

domain = "simonowens.tech"