terragrunt = {
  terraform {
    source = "git::https://gitlab.com/simonowens157/aws_modules.git//vpc?ref=v0.0.3"
    backend "s3" {}
  }
  # Include all settings from the root terraform.tfvars file
  include = {
    path = "${find_in_parent_folders()}"
  }
}
region = "us-east-1"
name = "dev-vpc"
cidr = "10.0.0.0/16"
azs             = ["us-east-1a", "us-east-1b"]
private_subnets = "10.0.1.0/24"
public_subnets  = "10.0.2.0/24"
enable_nat_gateway = true
enable_vpn_gateway = false

tags = {
 Terraform = "true"
 Environment = "dev"
}
