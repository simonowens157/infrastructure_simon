terragrunt = {
  remote_state {
    backend = "s3"
    
    config {
      encrypt        = true
      bucket         = "simon-static-website"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "us-east-1"
      dynamodb_table = "terraform-locks"
    }
  }
  terraform {
    
    extra_arguments "-var-file" {
      commands = ["${get_terraform_commands_that_need_vars()}"]
      optional_var_files = [
        "${get_tfvars_dir()}/${find_in_parent_folders("account.tfvars", "ignore")}",
      ]
    }
  }
}