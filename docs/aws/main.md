## What is
* VPC - https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html 
* ECS - need to make a aws_ecs_task_definition to declare my docker file. it needs some type of web task .json file where you specify how you want your container to run

## lessons learned
* terraform is still in its early stages - there are some bugs and weird hacks you will run into.
* use terragrunt for managing your workspace. workspaces are alternative.
* terragrunt allows you to: apply all, DRY backend, provider and folder layouts in general.
* terraform relative pathing is messed up
* use terratest to test all of your infrastructure
* use modules instead of repeating code everywhere
* make variables maps like below
variable "ami_id" {
  type = "map"

  default = {
    stg = "ami-foo28929"
    prd = "ami-bar39b12"
  }
}
image_id = "${lookup(var.ami_id, terraform.workspace)}"
* if you work with multiple people you should probably use a remote shared state. you can also use local tho which is good.
* you get output from other folders by looking at their state. you make a data terraform_remote_state resource.
* TF_VAR_ environment variables convert into callable variables in .tf files
* you can make *.auto.tfvars files for secret variables and exclude them with gitignore
* use terraform.tfvars only when you need to overwrite a default value. use default values in var.tf
* terraform.tfvars needs the format of: access_key = "foo"
* use git tagging: git tag v0.0.2
* refer to those tags in terraform: git::https://gitlab.com/simonowens157/infrastructure_simon.git//aws_modules/vpc?ref=v0.0.2
## Entire project
* iam - only need this for multiple people working on tf
* bastion host or CI server

## Per project
 * frontend
   * security group
   * elastic load balancer
   * ECS
 * backend
   * security gruop
   * ECS or their own DB
 * storage for remote state
 * dns
 * vpc

## Module links

* [VPC](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/1.60.0)

* [DNS management](https://registry.terraform.io/modules/cloudposse/route53-alias/aws/0.2.7)

* [Security group](https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/2.16.0)

* [Load balancer](https://registry.terraform.io/modules/terraform-aws-modules/elb/aws/1.4.1)

* [storage](https://registry.terraform.io/modules/cloudposse/s3-bucket/aws/0.3.0)

* [EC2 instance](https://registry.terraform.io/modules/terraform-aws-modules/ec2-instance/aws/1.21.0)

* [ECS](https://registry.terraform.io/modules/egarbi/ecs-service/aws/1.0.10)

* [auto scaling group](https://registry.terraform.io/modules/terraform-aws-modules/autoscaling/aws/2.9.1)

* [Wordpress site](https://registry.terraform.io/modules/soroushatarod/cloudfront-wordpress/aws/1.0.2)

* [Static site](https://registry.terraform.io/modules/buildo/website/aws/1.5.0)