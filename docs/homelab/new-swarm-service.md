### Procedure for adding a new docker service
1. create a folder/docker-compose.yml in "homelab" with the name of your service. name it "local.yml" if it is not a service
2. ssh to one of the nodes, create any volumes in /media/vol if necessary
3. start your service by being in "homelab" and running "./mainenance start <foldername>"
4. ssh to pihole, add dns name to /etc/pihole/lan.list then enter "sudo pihole restartdns"
5. navigate to your web service. use portainer to troubleshoot