## Notes for configure both Centos7 boxes
* these are my configuration notes getting a centos7 image on a server and making it a swarm node

### Bootable image
* Manager configured with Centos7. Used rufus to make bootable usb drive.
* Worker is a vm in esxi. Uploaded Centos7 iso.

### Configure DNS
* [Jump to my dns configuration page](/homelab/dns-setup/)

### Mount drives and configure NFS share
* find drives uuid with sudo blkid, mount them by uuid so they are the same upon restart
* [Config guide Mount NTFS drives](https://www.howtoforge.com/tutorial/mount-ntfs-centos/)
* [Config guide NFS](https://www.unixmen.com/setting-nfs-server-client-centos-7/)
* mounted to /media/3tb,/media/8tb, /media/vol - only accessible from .12
* fstab configured to auto mount upon restart

### Install Docker
* [Config Guide](https://github.com/NaturalHistoryMuseum/scratchpads2/wiki/Install-Docker-and-Docker-Compose-(Centos-7))

### Configure Docker Swarm with TLS and encrypted networks
* [Get wildcard cert for domain](https://finnian.io/blog/ssl-with-docker-swarm-lets-encrypt-and-nginx/) and ignore step 2) didn't hook this up to daemon
* [Configure docker swarm to use those certs](https://github.com/docker/docker.github.io/blob/master/swarm/configure-tls.md) do step 2 only
* use my scripts in vol/swarm/ to gen client, mv them to your dameon, and to configure your daemon

* Manager firewall rules:
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --add-port=8080/tcp --permanent
firewall-cmd --add-port=2376/tcp --permanent
firewall-cmd --add-port=2377/tcp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=4789/udp --permanent
firewall-cmd --reload
* Worker firewall rules:
firewall-cmd --add-port=2376/tcp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=4789/udp --permanent
firewall-cmd --reload

### Enesure the following work on restart
* Environment variables - in /etc/environment
* docker swarm (systemctl)
* docker containers (service auto restart 5 times)

### Docker Services Config with Terraform
* [Jump to my terraform configuration page](/homelab/terraform-docker/)