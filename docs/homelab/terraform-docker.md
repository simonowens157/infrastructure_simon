## Running all of my docker containers on swarm, configured by terraform
I am chosing to use terraform to manage my docker containers - partly because docker-compose files are messy and you can't easily do things like specify which docker host you want(you have to do -H tcp://host:port everytime you do a docker command). I also just want to become better with terraform because it seems like an up and coming tool.

### Terraform Failures
* After trying to get terraform to work with the docker provider for several hours I am giving up for now.  Below are my following reasons for sticking with docker-compose files
    * docker-compose files are much simpler and quicker for me, even though you are copy/pasting code alot. Its has been easy for me to maintain
    * it is extremely difficult to template terraform code that is readable, short, and flexible
    * successfully configuring terraform resources doesn't mean they work - and there isn't an easy way to test them. You can't test terraform code with terragrunt at the moment
    * terraform is immature - you can't use for loops, if statments, and other things - but this will be here very soon
* I am just going to write a bash script that hooks me up to my swarm remotely, and can easily deploy swarm services.

### Testing with go

### CI/CD rollerback and auto updates
