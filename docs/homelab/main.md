# Homelab Notes
This repository holds the documentation for my home lab I've built up and guides for others wanting
to build their own home network. Building your own home network is the best way to learn and can be
much cheaper than you think.  Below are my homelab notes

* [Hardware](/homelab/hardware/) - List of my resources that make this possible
* [Server Setup](/homelab/server-setup/) - Configuring DL380G7 as a docker swarm manager
* [DNS Setup](/homelab/dns-setup/) - Pihole and Unifi controller DNS setup
* [Add new node to swarm](/homelab/new-swarm-node/) - Procedure for adding another worker to the swarm
* [Add service to swarm](/homelab/new-swarm-service/) - Procedure for adding another service to the swarm
* [Terraform Docker swarm](/homelab/terraform-docker/) - Notes on using terraform to manage swarm services