## DNS Setup
* Namecheap domain: owens-netsec.com
* Unfi USG
* Pihole dns server

### IP
PI - .10
Manager - .11
Worker - .12
worker2 - .14

### Unifi
* Install and configure unifi software controller
* Assign Static Addresses
* configure domain name as owens-netsec.com
* configure dns server as pihole, wan as 1.1.1.1

## Pihole
* [Config guide](https://www.smarthomebeginner.com/pi-hole-tutorial-whole-home-ad-blocking/) skip to test 4
* [Setup to resolve hostnames](https://discourse.pi-hole.net/t/howto-using-pi-hole-as-lan-dns-server/533)
* Put the below in a file on the pi and restart
192.168.1.10    pihole.owens-netsec.com pihole
192.168.1.11    jackett.owens-netsec.com        jackett
192.168.1.11    sonarr.owens-netsec.com sonarr
192.168.1.11    radarr.owens-netsec.com radarr
192.168.1.11    transmission.owens-netsec.com   transmission
192.168.1.11    plex.owens-netsec.com   plex
192.168.1.11    ombi.owens-netsec.com   ombi
192.168.1.11    plexpy.owens-netsec.com plexpy
192.168.1.11    portainer.owens-netsec.com      portainer
192.168.1.11    media.owens-netsec.com  media
192.168.1.11    files.owens-netsec.com  files
192.168.1.11    traefik.owens-netsec.com        traefik
192.168.1.11    vpn.owens-netsec.com    vpn
192.168.1.11    runner.owens-netsec.com runner
192.168.1.11    docker-manager.owens-netsec.com docker-manager
192.168.1.12    docker-worker.owens-netsec.com  docker-worker
192.168.1.14    docker-worker2.owens-netsec.com  docker-worker2
192.168.1.9     unifi.owens-netsec.com  unifi
192.168.1.1     usg.owens-netsec.com    usg
192.168.1.69    esxi.owens-netsec.com   esxi

* restart command: sudo pihole restartdns

### Configure Hostname on new box
* [Configure hostname guide](https://phoenixnap.com/kb/how-to-set-or-change-a-hostname-in-centos-7)