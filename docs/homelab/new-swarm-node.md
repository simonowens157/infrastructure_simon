### Procedure for adding a new node to the swarm
1. SSH to pi, update /etc/pihole/lan.list, sudo pihole restartdns
2. turn on node(ssh and docker setup), ssh to new node, set hostname with: hostnamectl set-hostname *name*.owens-netsec.com
3. login to unifi controller, set static ip with that new node and name it
4. SSH to swarm manager, update /etc/exports with new ip, find swarm token with: "docker swarm join-token worker". restart manager
5. generate certs and move them into the right location on new node (/media/vol/certs/swarm). restart node.
6. join swarm