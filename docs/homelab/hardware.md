### My Hardware
Below is the hardware I use to run my home network.  My network works great for my needs, 
but by no means could support several users.  Building a network for enterprise use is a different beast.
Define what you need and want from your home lab before even thinking about buying hardware. Do lots of research. Many hardware buying guides from bloggers and streamers spend way too much money.  For example, if you are only going to store
files and host your own website, it makes no sense to spend $3,000 on a server with 52 GB of RAM. Over 3 years, I've spent approximately $1,600(the price of a nice laptop) on all tech related purchases(hardware, software, training) and have over 100gb ram, 20 cores, 11tb of slow storage, 1tb of fast storage, fast wifi/networking, personal websites/documentation(free using gitlab pages), cheap monthly spendings, and tons of experience as a result.
It's tempting to buy the latest and greatest hardware.  Always: 1.Ready  2.Aim  3.Fire

Monthly Spending
| Function             | Name                       | Cost $  |
| -------------        |:-------------:             | -----:  |
| TV                   | Sling                      | 30      |
| VPN                  | unamed                     | 3       |
| Music                | Amazon Music               | 8       |
| Fiber Internet       | AT&T                       | 90      |

Networking Equipment
| Function             | Name                       | Cost $  |
| -------------        |:-------------:             | -----:  |
| Router/Firewall      | Unifi USG                  | 112     |
| Switch               | Unifi 8 port POE Switch    | 102     |
| Wifi                 | Unifi UAC Pro              | 120     |
| Rack/Rails/Shelves   | 18U 19"                    | 190     |
| External Storage     | 8TB Seagate                | 130     |
| External Storage     | 3TB Seagate                | 80      |
| UPS                  | Tripp Lite 1500VA          | 160     |
| DNS/Add Block        | Raspberry Pi3              | 50      |
| TV Streamer, Antenna | AirTV, Antenna, Storage    | 200     |
| Firestick 4k         | Firestick 4k               | 40      |


Server 1(slow)
| Function             | Name                       | Cost $  |
| -------------        |:-------------:             | -----:  |
| Server               | HP DL 380 G7               | 100     |
| RAM                  | DDR3 SRAM 8GB x 6          | 16      |
| CPU                  | Xeon E5645 2.4 Ghz x 2     | 16      |
| Hard Drives          | 10 K SAS HP 300GB x 6      | 20      |

Server 2(fast)
| Function             | Name                       | Cost $   |
| -------------        |:--------------------:      | -------: |
| Server               | HP DL 380 G7               | 364      |
| RAM                  | DDR3 SRAM 16GB x 4         | included |
| CPU                  | X5687 2x4Core 3.60Ghz      | included |
| Hard Drives          | 15 K SAS HP 146GB x 4      | included |

Wifi Hacking Practice
| Function             | Name                       | Cost $  |
| -------------        |:-------------:             | -----:  |
| Router/Wifi          | Netgear WNR1000v2          | 30     |
| Wifi Hacking Adapter | ALFA Networks AWUS036H USB 500mW    | 20     |

### Configuration Guides(in progress)
[LINK](https://github.com/so87/Home-Lab/blob/master/Configuration%20Guides.md)

### Buying a Server and Hardware buying guide
 Use this [LINK](https://www.labgopher.com/) to buy servers.  It filters ebay for servers based on your inputs. <br />
 Reddit also has a great write-up for getting started <br />
 [Reddit Guide](https://www.reddit.com/r/homelab/wiki/buyingguide)
 
 Here is a great guide for making your own home NAS <br />
 [DIY NAS](https://blog.briancmoses.com/2017/03/diy-nas-2017-edition.html) 

# Community
This is a really great community for seeing what others do.  Great resource for building your
own homelab.  Becareful browsing this sub-reddit too much... it may make you buy expensive equipment. <br />
[Reddit Home Lab](https://www.reddit.com/r/homelab/)