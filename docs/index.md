## General Info
Hello! I am a security engineer that loves to make applications and infrastructure.  This is the documentation for my personal infrastructure.  I deploy things to gitlab pages, amazon web services, and my own homelab!
* Want to donate so I can keep making more opensource guides/tools? [Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=simonowens157%40gmail.com&item_name=Generating+free+technology+guides+and+tools&currency_code=USD&source=url)


I use the below technologies to do this!
* Docker: To package applications
* Packer: To build ec2 instances and Docker Containers
* Terraform: To declare infrastructure resources like: AWS networks, load balancers, and ec2 instances
* Terratest/Golang: to test my infrastructure
* Jekyll: To build static websites
* Mkdocs: For producing documentation
I talk about these technologies and practices and other things in my [blog](http://simonowens.tech).  Take a look at my other stuff and contact me if you have any questions!

### Other Repositories
* [Github](https://github.com/so87)
* [AWS Modules](https://gitlab.com/simonowens157/aws_modules)

### Websites
* [My personal site](http://simonowens.tech)

### Contact Info
* Email: simonowens157@gmail.com
* [LinkedIn](https://www.linkedin.com/in/simon-owens-206879126/)